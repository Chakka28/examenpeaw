import { Component, OnInit } from '@angular/core';
import { UserServiceService } from '../services/user-service.service';

@Component({
  selector: 'app-prueba',
  templateUrl: './prueba.component.html',
  styleUrls: ['./prueba.component.css']
})
export class PruebaComponent implements OnInit {

  private listUsers;
  constructor(private users: UserServiceService) { }

  ngOnInit() {
    this.obtenerUsers();
  }

  obtenerUsers(){
    this.users.GetUsers()
          .subscribe(
            res=>{
              console.log(res);
              this.listUsers = res;
            },
            err => this.ShowError(err.error["message"])
        )

  }
  ShowError(arg0: any): void {
    throw new Error("Method not implemented.");
  }

}
