import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {
  API_URI = 'https://localhost:5001/api/Users';
  constructor(private http: HttpClient) { }

  GetUsers(){
    return this.http.get(`${this.API_URI}`);
  }


}
